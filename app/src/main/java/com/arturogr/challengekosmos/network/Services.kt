package com.arturogr.challengekosmos.network

import com.arturogr.challengekosmos.domain.model.ResponseRickAndMorty
import com.arturogr.challengekosmos.utils.Constants
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

private val retrofit = Retrofit.Builder()
    .addConverterFactory(GsonConverterFactory.create())
    .baseUrl(Constants.URL_BASE)
    .build()

interface RickAndMortyApiService {

    @GET(Constants.URL_CHARACTER)
    suspend fun getData(@Query("page") type: Int):
            Response<ResponseRickAndMorty>
}

object RickAndMortyApi {
    val retrofitService : RickAndMortyApiService by lazy {
        retrofit.create(RickAndMortyApiService::class.java)
    }
}