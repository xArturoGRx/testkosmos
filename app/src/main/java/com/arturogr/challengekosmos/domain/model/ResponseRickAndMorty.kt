package com.arturogr.challengekosmos.domain.model

data class ResponseRickAndMorty(
    val info: Info,
    val results: List<Result>
)