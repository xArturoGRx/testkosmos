package com.arturogr.challengekosmos.domain.model

data class Origin(
    val name: String,
    val url: String
)