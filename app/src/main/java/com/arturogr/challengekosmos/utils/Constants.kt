package com.arturogr.challengekosmos.utils

object Constants {

    /**
     * url base
     */
    const val URL_BASE = "https://rickandmortyapi.com/api/"

    /**
     * url for service get character
     */
    const val URL_CHARACTER = "character/"

    /**
     * Empty list value
     */
    const val EMPTY_LIST = "La lista de resultado esta vacia"

    /**
     * Server failure
     */
    const val SERVER_FAILURE = "Ocurrio un fallo en el servicio"

}
