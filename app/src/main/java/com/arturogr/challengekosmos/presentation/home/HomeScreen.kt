package com.arturogr.challengekosmos.presentation.home

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import com.arturogr.challengekosmos.R.string
import com.arturogr.challengekosmos.domain.model.Result
import com.arturogr.challengekosmos.ui.components.MyItemUI
import com.arturogr.challengekosmos.ui.components.MyProgressUI
import com.arturogr.challengekosmos.ui.components.MyToolBarUI

@ExperimentalMaterial3Api
@Composable
fun HomeScreen(viewModel: HomeViewModel = hiltViewModel()) {
    val characters: List<Result> by viewModel.result.observeAsState(listOf())
    val isLoading: Boolean by viewModel.isLoading.observeAsState(true)

   Column {
       MyToolBarUI(string.app_name)
       if (isLoading) {
           MyProgressUI()
       }
       LazyColumn(modifier = Modifier.fillMaxSize()) {
           items(
               items = characters
           ) {character ->
               MyItemUI(character = character)
           }
       }
   }
}