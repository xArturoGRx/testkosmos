package com.arturogr.challengekosmos.presentation.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arturogr.challengekosmos.domain.model.Result
import com.arturogr.challengekosmos.network.RickAndMortyApi
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

@HiltViewModel
class HomeViewModel @Inject constructor() : ViewModel() {

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val _result = MutableLiveData<List<Result>>()
    val result: LiveData<List<Result>>
        get() = _result

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    init {
        search(1)
    }


    private fun search(page: Int) {
        _isLoading.value = true
        coroutineScope.launch {
            val response = RickAndMortyApi.retrofitService.getData(page)
            if (response.isSuccessful && response.body() != null) {
                _isLoading.value = false
                val dataResult = response.body()?.results
                dataResult?.let {
                    _result.value = it
                }
            }
        }
    }

}