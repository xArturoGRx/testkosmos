package com.arturogr.challengekosmos.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.arturogr.challengekosmos.R
import com.arturogr.challengekosmos.domain.model.Result


@Composable
fun MyItemUI(character: Result) {
    val isShow = remember { mutableStateOf(false) }
    val show = stringResource(id = R.string.show_detail)
    val hide = stringResource(id = R.string.hide_detail)
    val textButton = remember { mutableStateOf(show) }
    Card(modifier = Modifier
        .padding(5.dp)
        .semantics { contentDescription = "Item Result" },
        elevation = CardDefaults.cardElevation(4.dp),
        shape = RoundedCornerShape(10.dp),
        colors = CardDefaults.cardColors(Color.White)) {
        Row(
            horizontalArrangement = Arrangement.Start,
            modifier = Modifier
                .height(150.dp)
                .fillMaxWidth()
        ) {
            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(character.image)
                    .crossfade(true)
                    .build(),
                modifier = Modifier
                    .width(150.dp)
                    .height(150.dp),
                contentDescription = "Image",
                contentScale = ContentScale.Crop
            )
           Row {
               Column(
                   modifier = Modifier
                       .weight(1f)
                       .padding(top = 5.dp, start = 5.dp)
               ) {
                   MyTextUI(text = character.name, textSize = 20.sp)
                   if (isShow.value) {
                       textButton.value = hide
                       Column(){
                           MyTextUI(text = character.status)
                           MyTextUI(text = character.species)
                           MyTextUI(text = character.gender)
                           MyTextUI(text = character.origin.name)
                           MyTextUI(text = character.location.name)
                           MyTextUI(text = character.type)
                       }
                   } else {
                       textButton.value = show
                   }
               }
               Column(
                   modifier = Modifier
                       .fillMaxHeight()
                       .padding(end = 12.dp),
                   verticalArrangement = Arrangement.Center,
                   horizontalAlignment = Alignment.End
               ) {
                   MyButtonUI(text = textButton.value,
                       onClick = {
                           isShow.value = !isShow.value
                       })
               }
           }
        }
    }
}