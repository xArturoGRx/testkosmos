package com.arturogr.challengekosmos.ui.components

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.sp
import com.arturogr.challengekosmos.R.string
import com.arturogr.challengekosmos.ui.theme.Purple40

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MyToolBarUI(
    title: Int?,
    onBack:() -> Unit = {},
    upAvailable: Boolean = false,
) {
    TopAppBar(
        title = {
            Text(
                text = stringResource(id = title ?: string.app_name),
                fontSize = 18.sp,
                color = Color.White
            )
        },
        navigationIcon = {
            if (upAvailable) {
                IconButton(onClick = onBack ) {
                    Icon(
                        imageVector = Icons.Default.ArrowBack,
                        contentDescription = "TopBar",
                        tint = Color.White
                    )
                }
            }
        },
        colors = TopAppBarDefaults.smallTopAppBarColors(containerColor = Purple40)
    )
}
