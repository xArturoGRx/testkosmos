package com.arturogr.challengekosmos.ui.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp

@Composable
fun MyTextUI(text: String, textSize: TextUnit = 16.sp) {
    Column(
        modifier = Modifier.wrapContentWidth(),
        horizontalAlignment = Alignment.Start
    ) {
        Text(text = text,
            fontSize = textSize)
    }
}
